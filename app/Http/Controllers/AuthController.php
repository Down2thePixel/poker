<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * @var User
     */
    protected $users;

    public function __construct(User $users)
    {
        $this->users = $users;
    }

    public function login(Request $request)
    {
        $user = $this->users->whereEmail($request->email)->firstOrFail();

        if(Hash::check($request->password, $user->password) ) {
            $token = $user->createToken(null);

            return response()->json([
                'auth' => $token,
                'user' => $user
            ]);
        }

        return response()->json([
            'message' => 'Authentication failed.'
        ], 403);

    }
}
