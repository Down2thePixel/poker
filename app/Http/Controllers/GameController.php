<?php

namespace App\Http\Controllers;

use App\Events\{
    GameCreated,
    GameDeleted,
    JoinedGame
};
use App\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * @var Game
     */
    private $games;

    /**
     * GameController constructor.
     * @param Game $games
     */
    public function __construct(Game $games)
    {
        $this->games = $games;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Game $games
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->games->public()->get();
    }

    /**
     * Store a newley created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array_merge(
            $request->all(),
            [
                'game_id' => str_random(6)
            ]
        );

        $game = $request->user()->game()->create($data);

        event(new GameCreated($game));

        return $game;
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $game = $this->games->whereGameId($id)->first();

        event(new JoinedGame($game));

        return $game;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Game[]|\Illuminate\Database\Eloquent\Collection
     */
    public function destroy($id)
    {
        $this->games->findOrFail($id)->delete();

        event(new GameDeleted($id));

        return $this->games->all();
    }
}
