<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
        'game_id',
        'user_id',
        'name',
        'type',
        'starting_chips',
        'starting_blinds',
        'max_players',
        'blind_schedule',
        'blind_level',
        'rebuy_time',
        'status',
        'private',
    ];

    public $appends = [
        'userCanEdit'
    ];

    public function players()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get public games
     *
     * @param $query
     * @return mixed
     */
    public function scopePublic($query) {
        return $query->wherePrivate(0);
    }

    public function getUserCanEditAttribute()
    {
        return request()->user()->id === $this->user_id;
    }

//    public function setGameIdAttribute()
//    {
//        $this->attributes['game_id'] = str_random(6);
//    }

}
