let mix = require('laravel-mix');
mix.disableNotifications();

/**
 * Configuration
 */
require('dotenv').load({ path: __dirname + '/.env.frontend' });
const stringify = require('stringify-object-values');
const webpack = require('webpack');

//mix.options({ processCssUrls: true });
const config = {
  plugins: [new webpack.DefinePlugin({ 'process.env': stringify(process.env) })]
};

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css').webpackConfig(config).version();
