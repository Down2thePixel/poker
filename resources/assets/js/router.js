import Vue from 'vue';
import VueRouter from 'vue-router';

//import store from './store';
import { requireAuth } from './utils/auth';

import LoginView from './components/auth/Login';
import RegisterView from './components/auth/Register';
import LobbyView from './components/Lobby';
import GameView from './components/Game';

Vue.use(VueRouter);

export const routes = [
  { path: '', component: LobbyView, beforeEnter: requireAuth},
  { path: '/login', component: LoginView},
  { path: '/register', component: RegisterView},
  { path: '/game/:id',  component: GameView, beforeEnter: requireAuth},
  
  {path: '*', redirect: '/login'}
];

export default new VueRouter({mode: 'history', routes})