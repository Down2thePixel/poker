import axios from "axios/index";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');

axios.defaults.baseURL = process.env.API_URL;
axios.interceptors.request.use(function(config) {
  const token = localStorage.getItem('accessToken');
  
  if ( token != null ) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  
  return config;
}, function(err) {
  return Promise.reject(err);
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));
//Vue.component('Login', require('./components/Login.vue'));

import App from './App.vue';

import store from './store';
import router from './router';

const app = new Vue({
  el: '#app',
  data: {
    //appName: process.env.APP_NAME
  },
  store,
  router,
  render: h => h(App)
});


