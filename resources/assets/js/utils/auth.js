/**
 * Authentication
 */
import router from '../router';

export function requireAuth(to, from, next) {
  if (!isLoggedIn()) {
    next('/login');
  } else {
    next();
  }
}

export function logout() {
  localStorage.clear();
  router.push('/login');
}

export function getToken() {
  const token = localStorage.getItem('accessToken');
  const expires = localStorage.getItem('expires_at');
  
  return {
    accessToken: token,
    token_expires: expires
  }
}

export function setToken(payload) {
  localStorage.setItem('accessToken', payload.accessToken);
  localStorage.setItem('token_expires', payload.token.expires_at);
}

export function clearSession() {
  return localStorage.clear();
}

export function isLoggedIn() {
  const auth = getToken();
  return auth.accessToken !== null;
  //return !!idToken && !isTokenExpired(idToken);
}

// export function isTokenExpired(token) {
//   const expirationDate = getTokenExpirationDate(token);
//   return expirationDate < new Date();
// }