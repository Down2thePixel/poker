import Vue from 'vue'
import Vuex from 'vuex'

import {auth} from './modules/auth';
import game from './modules/game';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    appName: process.env.APP_NAME,
  },
  modules: {
    game,
    auth
  }
});