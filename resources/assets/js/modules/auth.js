import { setToken, getToken, logout } from "../utils/auth";
import Echo from 'laravel-echo'

const state = {
  accessToken: null,
  expires_at: null,
  user: null,
  username: null
}

const getters = {
  token (state) {
    return state.accessToken;
  },
  isAuthenticated (state){
    return state.accessToken !== null;
  },
  user (state){
    return state.user;
  },
  username(state){
    return state.username
  }
}

const mutations = {
  setUser (state, user){
    state.user = user;
    state.username = user.name;
  },
  setAuth (state, payload){
    state.accessToken = payload.accessToken;
    state.expires_at = payload.expires_at;
  },
  logout(state){
    state.accessToken = null;
    state.user = null;
    state.username = null;
    state.token_expires = null;
  }
}

const actions = {
  login ({commit}, payload) {
    setToken(payload.auth);
    commit('setAuth', payload.auth);
    commit('setUser', payload.user);
    //console.log(window.Echo);
    window.Echo.connector.pusher.config.auth.headers['Authorization'] = 'Bearer ' + payload.auth.accessToken;
  },
  logout({commit}){
    commit('logout');
    window.Echo.leave('users');
    logout();
  },
  tryAutoLogin ({commit}) {
    const auth = getToken();
    
    if (!auth.accessToken) {
      return;
    }
    commit('setAuth', auth);
    this.dispatch('getUser');
    // const expirationDate = localStorage.getItem('token_expiration')
    // const now = new Date()
    // if (now >= expirationDate) {
    //   return
    // }
    
  },
  getUser({commit}) {
    axios.get('/me').then((response) => {
      commit('setUser', response.data);
    }).catch((response) => {
      console.log(response);
    });
  }
}

export const auth = {
  state,
  getters,
  mutations,
  actions
}
