
const state = {
  game: {}
}

const getters = {
  game(state) {
    return state.game;
  }
}

const mutations = {
  setGame(state, game){
    state.game = game;
  }
}

const actions = {
  getGames(){
    let games = [];
    axios.get('/games').then( (response) => {
      games = response.data;
    }).catch((error) => {
      console.log(error);
    });
    
    return games;
  },
  getGame({commit}, id){
    let vm = this;
    axios.get(`/games/${id}`)
      .then((response) => {
        commit('setGame', response.data);
        return response.data;
      }).catch((error) => {
      console.log(error);
    });
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}