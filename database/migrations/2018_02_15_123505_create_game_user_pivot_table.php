<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_user', function (Blueprint $table) {
            $table->integer('game_id')->unsigned()->index();
            //$table->foreign('game_id')->references('id')->on('game')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            //$table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->primary(['game_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('game_user');
    }
}
