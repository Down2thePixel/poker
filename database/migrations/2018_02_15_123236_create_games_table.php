<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('game_id', 9);
            $table->string('name', 50);
            $table->enum('type', ['Tournament', 'Ring'])->default('Ring');
            $table->unsignedInteger('starting_chips')->default(1500);
            $table->string('starting_blinds')->default('5/10');
            $table->unsignedInteger('max_players')->default(5);
            $table->unsignedTinyInteger('blind_schedule')->default(15);
            $table->unsignedTinyInteger('blind_level')->default(1);
            $table->unsignedInteger('rebuy_time')->default(30);
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedTinyInteger('private')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('games');
    }
}
